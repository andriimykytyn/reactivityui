import { Component, OnInit, ViewChild } from '@angular/core';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { ChartOptions, ChartDataSets } from 'chart.js';
import { TodosService } from 'src/app/services/todos.service';
import { Todo } from 'src/app/models/Todo';
import { CategoryReport } from 'src/app/models/CategoryReport';

@Component({
    selector: 'app-datarange-status',
    templateUrl: './datarange-status.component.html',
    styleUrls: ['./datarange-status.component.css'],
  })
export class DatarangeStatusComponent implements OnInit {
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
    // { data: [180, 480, 770, 90, 1000, 270, 400], label: 'Series C', yAxisID: 'y-axis-1' }
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartLegend = true;
  public lineChartType = 'line';
  // public lineChartPlugins = [pluginAnnotations];

  todos: Todo[];

  constructor(private todoService: TodosService) { }

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
  
    ngOnInit() {
      this.todoService.getTodos().subscribe(todos => {
        this.todos = todos;
        let completionsByDate = {};
        let startDate = new Date('2019/9/15');
        let endDate = new Date('2019/10/02');

        let currentDate = new Date(startDate);
        this.lineChartLabels = []
        while(currentDate <= endDate) {
          let d = currentDate.setDate(currentDate.getDate() + 1);
          currentDate = new Date(d);

          var parsed = currentDate.toISOString().split('T')[0];

          completionsByDate[ parsed ] = // TODO test ensure it works with timezones
            new CategoryReport(0, 0);

            this.lineChartLabels.push( parsed );
        }

        this.todos.forEach(todo => {
          if(completionsByDate[todo.accomplishmentDate] != null) {
            completionsByDate[todo.accomplishmentDate].amount ++;
            completionsByDate[todo.accomplishmentDate].amountCompleted += todo.completed ? 1 : 0;
          }
        }); 

        var dataAll = []
        var dataCompleted = []
        
        Object.keys(completionsByDate).forEach(k => {
          dataAll.push( (completionsByDate[k].amountCompleted*100/(completionsByDate[k].amount+0.0000001)).toFixed(2))
          // dataCompleted.push()
        });
        console.log(dataAll)
        this.lineChartData[0].data = dataAll;
        this.lineChartData[0].label = "Percentage";

        // this.lineChartData[1].data = dataCompleted;
        // this.lineChartData[1].label = "Completed";
      });
    }
}