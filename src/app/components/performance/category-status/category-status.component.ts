import { Component, OnInit } from '@angular/core';
import { ChartType, ChartDataSets, RadialChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { CategoryService } from 'src/app/services/category.service';
import { Category } from 'src/app/models/Category';
import { DataService } from 'src/app/services/data.service';
import { Todo } from 'src/app/models/Todo';
import { TodosService } from 'src/app/services/todos.service';
import { CategoryReport } from 'src/app/models/CategoryReport';

@Component({
    selector: 'app-category-status',
    templateUrl: './category-status.component.html',
    styleUrls: ['./category-status.component.css'],
  })
export class CategoryStatusComponent implements OnInit {

    // Radar
  public radarChartOptions: RadialChartOptions = {
    scale: {
      pointLabels: {
        fontSize: 20,
        fontColor: "#000"
      }
    },
    legend: {
      display: false
    },
    responsive: true,
  };
  private categories: Category[] = [];
  private todos: Todo[] = [];
  private chartData: Array<number> = [];
  public radarChartLabels: Label[] = [];

  public radarChartData: any[] = [
    {data: this.chartData}
  ];
  public radarChartType: ChartType = 'radar';

  constructor(private categoryService: CategoryService, private todoService: TodosService) { }

  ngOnInit() {
    this.categoryService.getCategories().subscribe(response => {
      this.categories = response;
      this.categories.forEach(category => this.radarChartLabels.push(category.name));

      this.todoService.getTodos().subscribe(todos => {
        this.todos = todos;
        let categoriesById = {};
        this.categories.forEach(category => {
          category.categoryCompletion = new CategoryReport(0, 0);
          categoriesById[category.id] = category;
        });
        this.todos.forEach(todo => {
          if(categoriesById[todo.categoryId] != null) {
            categoriesById[todo.categoryId].categoryCompletion.amount ++;
            categoriesById[todo.categoryId].categoryCompletion.amountCompleted += todo.completed ? 1 : 0;
          }
        }); 
        var data = []
        this.categories.forEach(c => data.push( (c.categoryCompletion.amountCompleted*100/(c.categoryCompletion.amount+0.00001)).toFixed(2)));
        console.log(data)
        this.radarChartData[0].data = data;


      });

    });
  }

}