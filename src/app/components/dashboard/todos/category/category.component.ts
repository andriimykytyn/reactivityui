import { OnInit, Component, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { Category } from 'src/app/models/Category';
import { Todo } from 'src/app/models/Todo';
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.css'],
  })
export class CategoryComponent implements OnInit {

    categories: Category[] = [];
    @ViewChild('categoryName', {static: false}) categoryName: ElementRef;  


    constructor(private categoryService: CategoryService, private dataService: DataService) {
    }

    ngOnInit() {
      this.categoryService.getCategories().subscribe(data => {
        this.categories = data;
        this.dataService.changeCategories(this.categories);
      });
    }
  
    addCategory(category: string) {
      let categ = new Category(category);
      console.log(categ);
      this.categoryService.createCategory(categ).subscribe(cat => {
        this.categories.push(cat);
        this.dataService.changeCategories(this.categories);
      });
      this.categoryName.nativeElement.value = '';
    }
    
    removeCategory(id: number) {
      let category = this.categories.find(categories => categories.id === id).name;
      if (confirm("Are you sure to delete category "+category+ "?")) {
        this.categoryService.removeCategory(id).subscribe();
        this.categories = this.categories.filter(categories => categories.id !== id);
        this.dataService.changeCategories(this.categories);
      }
    }
    
    updateCategory(id: number, value: HTMLInputElement) {
      let item = this.categories.find(categories => categories.id === id);
      item.name = value.toString();
      this.categoryService.updateCategory(id, item).subscribe();
      this.dataService.changeCategories(this.categories);
    }

    activateEdit(id: number) {
      let item = this.categories.find(categories => categories.id === id);
      item.enabled = !item.enabled;
    }

    toggleCategory(id: number) {
      let item = this.categories.find(categories => categories.id === id);
      item.active = !item.active;
    }

}