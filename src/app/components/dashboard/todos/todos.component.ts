import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';
import { TodoPriority } from 'src/app/models/TodoPriority';
import { Todo } from 'src/app/models/Todo';
import { DataService } from 'src/app/services/data.service';
import { Category } from 'src/app/models/Category';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
  providers: [TodosService]
})
export class TodosComponent implements OnInit {
  
  priorities = (<any>Object.keys(TodoPriority)).filter(e => e != +e);
  todos: Todo[] = [];
  categories: Category[] = [];
  // @Output() todosEmit = new EventEmitter<any>();
  @ViewChild('todoTitle', {static: false}) todoTitle: ElementRef;  
  @ViewChild('todoDate', {static:false}) todoDate: ElementRef;


  onClick(todo: Todo){
      todo.showDetails = !todo.showDetails;
  }

  constructor(private todoService: TodosService, private dataService: DataService) { }

  ngOnInit() {
    this.todoService.getTodos().subscribe(data => {
      this.todos = data;
      this.dataService.changeTodos(this.todos);
    });
    this.dataService.currentCategories.subscribe(categories => {
      this.categories = categories;
    });
    // this.todosEmit.emit(this.todos);
  }

  addTodo(title: string) {
    this.todoService.createTodo(new Todo(title)).subscribe(todo => {
      this.todos.push(todo);
      this.dataService.changeTodos(this.todos);

    // this.todosEmit.emit(this.todos);
    });
    this.todoTitle.nativeElement.value = '';
  }

  removeTodo(id: number) {
    this.todoService.removeTodo(id).subscribe();
    this.todos = this.todos.filter(todo => todo.id !== id);
    this.dataService.changeTodos(this.todos);
  }

  completeTodo(id: number) {
    let item = this.todos.find(todo => todo.id === id);
    item.completed = !item.completed;
    this.todoService.editTodo(id, item).subscribe();
    // this.todosEmit.emit(this.todos);
    this.dataService.changeTodos(this.todos);

  }

  removeAll() {
    if (confirm("Are you sure to delete all?")) {
      this.todos = [];
      this.todoService.removeAll().subscribe();
      this.dataService.changeTodos(this.todos);

    }
    // this.todosEmit.emit(this.todos);

  }

  editTitle(id: number) {
    let item = this.todos.find(todo => todo.id === id);
    item.title = (<HTMLInputElement>document.getElementById(id.toString())).value;
    console.log(id);
    this.todoService.editTodo(id, item).subscribe();
  }

  editNotes(id: number) {
    let item = this.todos.find(todo => todo.id === id);
    item.notes = (<HTMLInputElement>document.getElementById('notes'+id.toString())).value;
    this.todoService.editTodo(id, item).subscribe();
  }

  changePriority(id: number, priority: string) {
    let item = this.todos.find(todo => todo.id === id);
    item.priority = TodoPriority[priority];
    this.todoService.editTodo(id, item).subscribe();
    this.dataService.changeTodos(this.todos);
  }

  setAccomplishmentDate(id: number){
    let item = this.todos.find(todo => todo.id === id);
    item.accomplishmentDate = (<HTMLInputElement>document.getElementById("date"+id.toString())).value
    this.todoService.editTodo(id, item).subscribe();
    this.dataService.changeTodos(this.todos);
  }

  changeCategory(todo:Todo, categoryName:string) {
    this.categories.find(category => {
      if (category.name === categoryName) {
        todo.categoryId = category.id;
      } 
    });
    if (categoryName === "Choose category") {
      todo.categoryId = null;
    }
    this.todoService.changeCategory(todo.id, todo).subscribe();
  }

}
