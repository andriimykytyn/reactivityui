import { Component, OnInit, Input } from '@angular/core';
import { TodosService } from '../../../services/todos.service'
import { Report } from 'src/app/models/Report';
import { MultiDataSet } from 'ng2-charts';
import { Todo } from 'src/app/models/Todo';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css'],
  providers: [TodosService]
})
export class StatisticComponent implements OnInit {
  // @Input() todos: Todo[]
  todos: Todo[] = [];
  report: Report = new Report;
  public doughnutChartLabels = ['Completed', 'Uncompleted'];
  chartData = [{ data: [0, 10], label: ['Completed', 'Uncompleted'], color: ['red', 'green'] }];

  doughnutChartType = 'doughnut';
  
  
  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.currentTodos.subscribe(todos => {
      this.todos = todos;
      let completed = 0;
      let uncompleted = 0;
      for (var i = 0; i < this.todos.length; i++) {
        if (this.todos[i].completed) {
          completed++;
        }
        if (!this.todos[i].completed) {
          uncompleted++
        }  
      }
      this.chartData[0].data = [completed, uncompleted];
    });
    // this.todoService.getByDate().subscribe(data => {
    //   this.report = data;
    //   this.chartData = [{ data: [this.report.completed, this.report.uncompleted], label: ['Completed', 'Uncompleted'], color: ['red', 'green'] }];
    // });
  }

  // catchTodos($event) {
  //   this.todos = $event;
    // let completed = 0;
    // let uncompleted = 0;
    // for (var i = 0; i < this.todos.length; i++) {
    //   if (this.todos[i].completed) {
    //     completed++;
    //   }
    //   if (!this.todos[i].completed) {
    //     uncompleted++
    //   }  
    // }
    // this.chartData = [{ data: [completed, uncompleted], label: ['Completed', 'Uncompleted'], color: ['red', 'green'] }];
  // }



}
