import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { RouterModule, Routes } from '@angular/router';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodosComponent } from './components/dashboard/todos/todos.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { StatisticComponent } from './components/dashboard/statistic/statistic.component';
import { HeaderComponent } from './components/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CategoryComponent } from './components/dashboard/todos/category/category.component';
import { PerformanceComponent } from './components/performance/performance.component';
import { DatarangeStatusComponent } from './components/performance/datarange/datarange-status.component';
import { CategoryStatusComponent } from './components/performance/category-status/category-status.component';

const appRoutes: Routes = [
  { path: 'todos', component: DashboardComponent },
  { path: '', redirectTo: '/todos', pathMatch: 'full' },
  { path: 'dashboard', component: PerformanceComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    StatisticComponent,
    HeaderComponent,
    DashboardComponent,
    CategoryComponent,
    PerformanceComponent,
    DatarangeStatusComponent,
    CategoryStatusComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ChartsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
