import { TodoPriority } from './TodoPriority';
import { Category } from './Category';

export class Todo {
    id: number;
    title: string;
    notes: string;
    priority: TodoPriority;
    routine: boolean;
    completed: boolean;
    createdAt: string;
    accomplishmentDate: string;
    showDetails: boolean;
    categoryId: number

    constructor(title: string) {
        this.title = title;
    }
}