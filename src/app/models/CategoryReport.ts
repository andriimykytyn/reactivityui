export class CategoryReport {
    amount: number;
    amountCompleted: number;

    constructor(amount: number, amountCompleted: number) {
        this.amount = amount;
        this.amountCompleted = amountCompleted;
    }
}