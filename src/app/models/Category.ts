import { CategoryReport } from './CategoryReport';

export class Category {
    id: number;
    name: string;
    enabled: boolean;
    active: boolean;
    categoryCompletion: CategoryReport;

    constructor(name: string) {
        this.name = name;
    }
}