import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../models/Todo';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  removeTodo(id: number) {
    return this.http.delete("http://localhost:8082/todos/"+id);
  }

  constructor(private http: HttpClient) { }

  getTodos():any {
    return this.http.get("http://localhost:8082/todos");
  }

  getByDate():any {
    let today = new Date();
    let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    // console.log(date);
    return this.http.get("http://localhost:8082/todos/date?from="+date);
  }

  createTodo(todo: Todo):any {
    return this.http.post("http://localhost:8082/todos", todo);
  }

  editTodo(id: number, item: Todo) {
    return this.http.put("http://localhost:8082/todos/"+id, item);
  }

  removeAll() {
    return this.http.delete("http://localhost:8082/todos");
  }

  changeCategory(todoId:number, item: Todo):any {
    return this.http.put("http://localhost:8082/todos/"+todoId, item);
  }

}
