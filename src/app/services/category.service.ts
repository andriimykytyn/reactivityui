import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from '../models/Category';

@Injectable({
    providedIn: 'root'
  })
export class CategoryService {
  constructor(private http: HttpClient) { }

  getCategories():any {
    return this.http.get("http://localhost:8082/category");
  }

  createCategory(category: Category):any {
    return this.http.post("http://localhost:8082/category", category);
  }

  removeCategory(id: number):any {
    return this.http.delete("http://localhost:8082/category/"+id);
  }

  updateCategory(id: number, category: Category):any {
    return this.http.put("http://localhost:8082/category/"+id, category);
  }
}