import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Todo } from '../models/Todo';
import { Category } from '../models/Category';

@Injectable({
    providedIn: 'root'
  })
export class DataService {
    private todos = new BehaviorSubject<Array<any>>([]);
    private categories = new BehaviorSubject<Array<any>>([]);
    
    currentTodos = this.todos.asObservable();
    currentCategories = this.categories.asObservable();
    constructor() { }

    changeTodos(todos: Todo[]) {
        this.todos.next(todos);
    }

    changeCategories(categories: Category[]) {
      this.categories.next(categories);
    }
}