import { Component, Output, EventEmitter } from '@angular/core';
import { TodosService } from './services/todos.service';
import { Todo } from './models/Todo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}
